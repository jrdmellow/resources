# Programming

* [Online](#markdown-header-online)
* [UI](#markdown-header-ui)
* [Gameplay](#markdown-header-gameplay)
* [Online](#markdown-header-graphics)

### Online

* [Determinism in League of Legends: Unified Clock](https://engineering.riotgames.com/news/determinism-league-legends-unified-clock)

### UI

### Gameplay

* [Bytecode (Virtual Machine for Scripting)](http://gameprogrammingpatterns.com/bytecode.html)

### Graphics

* Frame Analysis
    * [A Trip Down the LoL Graphics Pipeline](https://engineering.riotgames.com/news/trip-down-lol-graphics-pipeline)
    * [GTA V - Graphics Study](http://www.adriancourreges.com/blog/2015/11/02/gta-v-graphics-study/)
    * [DOOM - Graphics Study](http://www.adriancourreges.com/blog/2016/09/09/doom-2016-graphics-study/)